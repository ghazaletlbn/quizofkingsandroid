package com.example.quizofkings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quizofkings.Seializeclasses.User;
import com.example.quizofkings.UI.Home.Homefragment;
import com.example.quizofkings.UI.Profile.Profilefragment;
import com.example.quizofkings.UI.Settings.Settingfragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeActivity extends AppCompatActivity {
    private  BottomNavigationView bottomNavigationView;
    static User user;
  Button button;
  TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bottomNavigationView = this.findViewById(R.id.button_nav);
        Homefragment homefragment = new Homefragment();
        openfragment(homefragment);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navhome:
                        Toast.makeText(getApplicationContext(), "clicked on home", Toast.LENGTH_LONG).show();
                        Homefragment homefragment = new Homefragment();
                        openfragment(homefragment);
                        break;
                    case R.id.navprofile:
                        Toast.makeText(getApplicationContext(), "clicked on profile", Toast.LENGTH_LONG).show();
                        Profilefragment profilefragment = new Profilefragment();
                        openfragment(profilefragment);
                        break;
                    case R.id.navsetting:
                        Toast.makeText(getApplicationContext(), "clicked on settings", Toast.LENGTH_LONG).show();
                        Settingfragment settingfragment = new Settingfragment();
                        openfragment(settingfragment);
                        break;

                }
                return false;
            }
        });
        /*
        button = findViewById(R.id.buttonprofile);
        textView = findViewById(R.id.textViewpro);
        button button.setOnClickListener(v -> {
            textView.setText("doneee");
        });
    }

         */
    }
    private void openfragment(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
       // transaction.addToBackStack(null);
        transaction.commit();
    }
}