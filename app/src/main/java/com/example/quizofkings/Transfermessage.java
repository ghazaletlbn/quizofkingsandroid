package com.example.quizofkings;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.example.quizofkings.Seializeclasses.User;
import com.example.quizofkings.UI.Settings.Settingfragment;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Transfermessage extends AsyncTask<String , Void , Void> {
    public Socket socket;
    public ObjectOutputStream outputStream;
    public ObjectInputStream inputStream;
    public String message;
    static String msg;
    @SuppressLint("StaticFieldLeak")
    Context myContext;
    @SuppressLint("StaticFieldLeak")
    Activity origin;
     boolean done =false;
     public boolean getdone(){
         return done;
     }
    public Transfermessage(Context appcontext) {

        myContext=appcontext;
        origin = (Activity) myContext;
    }


    @Override
    protected  Void doInBackground(String... voids) {
        String s = voids[0];

        try {
            socket = new Socket("10.0.2.2", 8850);
            new  Thread(() -> {
                try {
                    outputStream = new ObjectOutputStream(socket.getOutputStream());
                    outputStream.writeUTF(s);
                    Log.e("SEND::" , s);
                    outputStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
            new Thread(() -> {
                try {
                    inputStream = new ObjectInputStream(socket.getInputStream());
                    message = inputStream.readUTF();
                    Log.e("SERVER" ,  message);
                    msg = message;

                    String[] params = message.split(":");
                    switch (params[0]) {
                        case "signUp": {
                            if (params[1].equals(WelcomActivity.username)){
                                if (params[2].equals("success")) {
                                    Log.e("SERVER" ,  params[2] + " " +RegisterActivity.result);
                                    WelcomActivity.username = RegisterActivity.editTextUsername.getText().toString();
                                    RegisterActivity.u = new User(WelcomActivity.username, RegisterActivity.editTextPassword.getText().toString());

                                    Intent intent = new Intent(origin, HomeActivity.class);
                                    myContext.startActivity(intent);

                                } else if (!params[2].equals("error")) {
                                    RegisterActivity.result = "ERROR";
                                }
                                //done = true;

                            }
                            break;
                        }
                        case "userChecker": {
                            RegisterActivity.result = params[2];

                            Log.e("tag User Checker", message);
                            break;
                        }
                        case "signIn": {
                            if (params[2].equals("success")) {
                                SigninActivity.signinusername = SigninActivity.username.getText().toString();
                                WelcomActivity.username = SigninActivity.username.getText().toString();
                                Intent signintent = new Intent(origin, HomeActivity.class);
                                try {
                                   RegisterActivity.u = (User) inputStream.readObject();
                                    Log.e("objectrecive",  RegisterActivity.u.getUsername() );

                                } catch (ClassNotFoundException e) {
                                    e.printStackTrace();
                                }
                                myContext.startActivity(signintent);
                            //Toast.makeText(origin, SigninActivity.username.getText().toString() + "welcome Back!", Toast.LENGTH_LONG).show();
                            } else {
                                SigninActivity.result = "ERROR";
                            }
                            done = true;
                            break;
                        }
                        case "userchange": {
                            if (params[2].equals("success")) {
                                SigninActivity.signinusername = Settingfragment.username;
                                WelcomActivity.username = Settingfragment.username;
                                Settingfragment.back();
                            } else {
                                Settingfragment.changetext.setError("it's not change");
                            }
                            done = true;
                            break;
                        }


                        default:
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }
}
