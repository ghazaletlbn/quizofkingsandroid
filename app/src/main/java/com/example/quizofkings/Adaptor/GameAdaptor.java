package com.example.quizofkings.Adaptor;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quizofkings.R;
import com.example.quizofkings.Seializeclasses.Game;
import com.example.quizofkings.UI.Home.Homefragment;
import com.example.quizofkings.WelcomActivity;

import java.util.List;

public class GameAdaptor extends RecyclerView.Adapter< GameAdaptor.Waitingviewholder>  {
    private OnItemClickListener mListener;
    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }
    private List<Game> games;
    private Homefragment activity;

    public GameAdaptor(Homefragment activity) {
        this.activity = activity;
    }

    public void setGames(List<Game> waitinggames) {
        this.games = waitinggames;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Waitingviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.games,parent,false);
        return new Waitingviewholder(view,mListener);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull final Waitingviewholder holder, final int position) {

        Game game = games.get(position);
        if(game.getGamer1().getUsername().equals(WelcomActivity.username)) {
            holder.Opponentusername.setText(game.getGamer2().getUsername());
        }
        else{
            holder.Opponentusername.setText(game.getGamer1().getUsername());
        }




    }

    @Override
    public int getItemCount() {
        return games.size();
    }

    public static class Waitingviewholder extends RecyclerView.ViewHolder{
        public TextView Opponentusername;
        public TextView Opponentscore;
        public TextView yourscore;
        CardView holdercard;
        public Waitingviewholder(@NonNull final View itemView, final OnItemClickListener listener) {
            super(itemView);
            Opponentusername = (TextView) itemView.findViewById(R.id.Rivalusername);
            Opponentscore = (TextView)  itemView.findViewById(R.id.Rivalscore);
            yourscore = (TextView)  itemView.findViewById(R.id.yourscore);
            holdercard = (CardView) itemView.findViewById(R.id.holdercardview);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });


        }
    }


}

