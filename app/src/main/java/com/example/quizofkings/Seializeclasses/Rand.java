package com.example.quizofkings.Seializeclasses;

import java.io.Serializable;
import java.util.Random;

public class Rand implements Serializable {
    Subject subject;
    Question[] questions;

    public Rand(Subject subject) {
        this.subject = subject;
        int questioncount = subject.getQuestions().size();
        Random random =new Random();
        int num1 = random.nextInt(questioncount);
        int num2 = random.nextInt(questioncount);
        int num3 = random.nextInt(questioncount);
        if(( num1 ==num2 )||(num1==num3)||(num2==num3)){
            while(num1==num2){
                num2=random.nextInt(questioncount);
            }
            while((num1==num3)||(num2==num3)){
                num3=random.nextInt(questioncount);
            }
        }
        questions = new Question[]{subject.getQuestions().get(num1), subject.getQuestions().get(num2), subject.getQuestions().get(num3)};

    }
}
