package com.example.quizofkings.Seializeclasses;

import java.io.Serializable;
import java.util.List;

public class Game implements Serializable {
    List<Subject> subjects;
    User gamer1;
    User gamer2;
    int gamer1score;
    int gamer2score;

    public Game(List<Subject> subjects, User gamer1, User gamer2) {
        this.subjects = subjects;
        this.gamer1 = gamer1;
        this.gamer2 = gamer2;
        this.gamer1score = 0;
        this.gamer2score =0;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public User getGamer1() {
        return gamer1;
    }

    public void setGamer1(User gamer1) {
        this.gamer1 = gamer1;
    }

    public User getGamer2() {
        return gamer2;
    }

    public void setGamer2(User gamer2) {
        this.gamer2 = gamer2;
    }

    public int getGamer1score() {
        return gamer1score;
    }

    public void setGamer1score(int gamer1score) {
        this.gamer1score = gamer1score;
    }

    public int getGamer2score() {
        return gamer2score;
    }

    public void setGamer2score(int gamer2score) {
        this.gamer2score = gamer2score;
    }
}
