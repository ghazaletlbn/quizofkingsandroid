package com.example.quizofkings.Seializeclasses;

import java.io.Serializable;
import java.util.ArrayList;

public class Subject implements Serializable {
    String name;
    ArrayList<Question> questions;

    public Subject(String name) {
        this.name = name;
        this.questions = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void addQuestions(Question question) {
        this.questions.add(question);
    }
}
