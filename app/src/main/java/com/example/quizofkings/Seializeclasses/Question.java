package com.example.quizofkings.Seializeclasses;

import java.io.Serializable;

public class Question implements Serializable {
    String questiontext;
    String correctanswer;
    String[] wronganswers ;

    public Question(String questiontext, String correctanswer,String wronganswer1 , String wronganswer2 , String wronganswer3) {
        this.questiontext = questiontext;
        this.correctanswer = correctanswer;
        this.wronganswers = new String[]{wronganswer1, wronganswer2, wronganswer3};
    }

    public String getQuestiontext() {
        return questiontext;
    }

    public String getCorrectanswer() {
        return correctanswer;
    }

    public String[] getWronganswers() {
        return wronganswers;
    }
}
