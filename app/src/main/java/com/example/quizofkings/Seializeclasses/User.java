package com.example.quizofkings.Seializeclasses;

import java.io.Serializable;
import java.util.*;

public class User implements Serializable {
    String username;
    String password;
    int coins;
    int score;
    List<Game> waitingforopponent;
    List<Game> yourturn;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        coins = 0;
        score = 0;
        waitingforopponent = new ArrayList<>();
        yourturn = new ArrayList<>();

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCoins() {
        return coins;
    }

    public void addCoins(int coins) {
        this.coins += coins;
    }
    public void removeCoins(int coins) {
        this.coins -= coins;
    }

    public int getScore() {
        return score;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public List<Game> getWaitingforopponent() {
        return waitingforopponent;
    }

    public void addWaitingforopponent(Game game) {
        waitingforopponent.add(game);
    }

    public List<Game> getYourturn() {
        return yourturn;
    }

    public void addYourturn(Game game) {
        yourturn.add(game);
    }
}
