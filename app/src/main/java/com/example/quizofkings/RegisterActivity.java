package com.example.quizofkings;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quizofkings.Seializeclasses.User;

import java.util.concurrent.ExecutionException;

public class RegisterActivity extends AppCompatActivity {
    public static User u;
    public static String result = "";
    static String username;
    boolean onTime = false;
    boolean uniqe = false;
    boolean blank = true;
    boolean confirm = false;

    @SuppressLint("StaticFieldLeak")
    static EditText editTextUsername;
    @SuppressLint("StaticFieldLeak")
    static EditText editTextPassword;
    EditText edittextconfirmpass;
    Button buttonRegister;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        editTextUsername = findViewById(R.id.regUsername);
        editTextPassword = findViewById(R.id.regPassword);
        edittextconfirmpass = findViewById(R.id.regconfirmPassword);
        buttonRegister = findViewById((R.id.buttonregister));
        textView = findViewById(R.id.textView);

        editTextUsername.setOnFocusChangeListener((v1, hasFocus) -> {
            if (!editTextUsername.getText().toString().equals("")) {
                String check = "userChecker:" + editTextUsername.getText().toString();
                Transfermessage transferMessage = new Transfermessage(RegisterActivity.this);
                transferMessage.execute(check);
                if (RegisterActivity.result.equals("repeated") ) {
                    RegisterActivity.result = "";
                    //editTextUsername.requestFocus();
                    RegisterActivity.editTextUsername.setError("Repeated Username");
                }
                Toast.makeText(RegisterActivity.this, transferMessage.message, Toast.LENGTH_SHORT).show();
                //textView.setText(result);
            }

            else{
                blank = true;
            }
        });

        editTextPassword.setOnFocusChangeListener((v2, hasFocus) -> {
            if (editTextUsername.getText().toString().equals("")) {
                editTextUsername.setError("Please Enter Username");
                blank = true;
            }
            if(editTextPassword.getText().toString().length()<5 ){
                blank = true;
            }

        });
        edittextconfirmpass.setOnFocusChangeListener((v3, hasFocus) -> {
            if (editTextUsername.getText().toString().equals("")) {
                editTextUsername.setError("Please Enter Username");
                blank = true;
            }
            if (editTextPassword.getText().toString().equals("")) {
                editTextPassword.setError("Please Enter Username");
                blank = true;
            }
            if(editTextPassword.getText().toString().length()<5){
                blank = true;
                editTextPassword.setError("Password should be at least 5 characters");
            }

        });

/*
        buttonRegister.setOnClickListener(v -> {
            textView.setText("doneee");
        });

 */

    }


    public void buttonRegisterOnClick(View view) {

        if (editTextUsername.getText().toString().equals("")) {
            editTextUsername.setError("Please Enter Username");
        } else if (editTextPassword.getText().toString().length() < 5) {
            editTextPassword.setError("Password should be at least 5 characters");
        } else if (edittextconfirmpass.getText().toString().equals("")) {
            edittextconfirmpass.setError("please Enter ConfirmPassword");
        } else if (!editTextUsername.getText().toString().equals("") && !editTextPassword.getText().toString().equals("") && edittextconfirmpass.getText().toString().equals(editTextPassword.getText().toString())) {
            WelcomActivity.username = editTextUsername.getText().toString();
            String check2 = "signUp:" + editTextUsername.getText().toString() + ":" + editTextPassword.getText().toString();
            Transfermessage transferMessage2 = new Transfermessage(RegisterActivity.this);
            transferMessage2.execute(check2);


        }
        else{
            edittextconfirmpass.setError("is not the same as a password");
        }


    }
}


