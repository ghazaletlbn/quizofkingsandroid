package com.example.quizofkings.UI.Settings;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.quizofkings.HomeActivity;
import com.example.quizofkings.R;
import com.example.quizofkings.RegisterActivity;
import com.example.quizofkings.SigninActivity;
import com.example.quizofkings.Transfermessage;
import com.example.quizofkings.WelcomActivity;

public class Settingfragment extends Fragment {
    @SuppressLint("StaticFieldLeak")
    static Button change,userchange,passchange;
    @SuppressLint("StaticFieldLeak")
    public static EditText changetext;
    static boolean userch = false;
    static boolean passch = false;
    public static String username;
    public static String password;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_setting,container,false);
        change = (Button) root.findViewById(R.id.changebutton);
        userchange = (Button) root.findViewById(R.id.changeusername);
        passchange = (Button) root.findViewById(R.id.changepassword);
        changetext = (EditText) root.findViewById(R.id.changetext);
        userchange.setOnClickListener(v ->{
            userchange.setVisibility(View.INVISIBLE);
            passchange.setVisibility(View.INVISIBLE);
            change.setVisibility(View.VISIBLE);
            changetext.setVisibility(View.VISIBLE);
            userch = true;
            passch = false;
            username = changetext.getText().toString();
            changetext.setOnFocusChangeListener((v2, hasFocus) -> {
                if (!changetext.getText().toString().equals("")) {
                    String check = "userChecker:" + changetext.getText().toString();
                    Transfermessage transferMessage = new Transfermessage(getActivity());
                    transferMessage.execute(check);
                    if (RegisterActivity.result.equals("repeated") ) {
                        RegisterActivity.result = "";
                        //editTextUsername.requestFocus();
                        changetext.setError("Repeated Username");
                    }
                }

                if (changetext.getText().toString().equals("")) {
                   changetext.setError("Please Enter Username");
                }



            });

        });
        ////////
        userchange.setOnClickListener(v ->{
            userchange.setVisibility(View.INVISIBLE);
            passchange.setVisibility(View.INVISIBLE);
            change.setVisibility(View.VISIBLE);
            changetext.setVisibility(View.VISIBLE);
            passch = true;
            userch = false;

        });
        /////////////
        changetext.setOnFocusChangeListener((v2, hasFocus) -> {
            if (!changetext.getText().toString().equals("")&& userch) {
                String check = "userChecker:" + changetext.getText().toString();
                Transfermessage transferMessage = new Transfermessage(getActivity());
                transferMessage.execute(check);

            }



        });
        /////////////
        change.setOnClickListener(v ->{
            if (changetext.getText().toString().equals("")) {
                changetext.setError("Please Enter Username");
            }

            else if (RegisterActivity.result.equals("repeated") ) {
                RegisterActivity.result = "";
                changetext.setError("Repeated Username");
            }
            else if(userch){
                String check = "userchange:" + WelcomActivity.username +changetext.getText().toString();
                Transfermessage transferMessage = new Transfermessage(getActivity());
                transferMessage.execute(check);

            }

        });
        return root;
    }
    public static void back(){
        userchange.setVisibility(View.VISIBLE);
        passchange.setVisibility(View.VISIBLE);
        change.setVisibility(View.INVISIBLE);
        changetext.setVisibility(View.INVISIBLE);
        passch = false;
        userch = false;
    }
}
