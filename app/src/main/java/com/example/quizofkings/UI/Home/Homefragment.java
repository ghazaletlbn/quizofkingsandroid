package com.example.quizofkings.UI.Home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quizofkings.Adaptor.GameAdaptor;
import com.example.quizofkings.GamepageActivity;
import com.example.quizofkings.R;
import com.example.quizofkings.RegisterActivity;
import com.example.quizofkings.Seializeclasses.Game;
import com.example.quizofkings.Seializeclasses.Subject;
import com.example.quizofkings.Seializeclasses.User;
import com.example.quizofkings.WelcomActivity;

import java.util.ArrayList;
import java.util.List;

public class Homefragment extends Fragment {
    static List<Game> waitinggames = new ArrayList<>();
    static List<Game> yourturngames = new ArrayList<>();

    private RecyclerView waitingrecyclerView;
    private RecyclerView yourturnrecyclerView;
    static GameAdaptor waitingAdaptor;
    static GameAdaptor yourturnAdaptor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home,container,false);
        waitingrecyclerView = (RecyclerView) root.findViewById(R.id.waitingsgamesrecycler_view);
        yourturnrecyclerView = (RecyclerView) root.findViewById(R.id.yourturngamesrecycler_view);
        waitingrecyclerView.setHasFixedSize(true);
        yourturnrecyclerView.setHasFixedSize(true);
        waitingAdaptor = new GameAdaptor(Homefragment.this);
        yourturnAdaptor = new GameAdaptor(Homefragment.this);
        waitingrecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        waitingrecyclerView.setAdapter(waitingAdaptor);
        yourturnrecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        yourturnrecyclerView.setAdapter(yourturnAdaptor);
        waitingAdaptor.setOnItemClickListener(new GameAdaptor.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                String rivalid =  waitinggames.get(position).getGamer1().getUsername(); //edit this
                String yourscore =Integer.toString(waitinggames.get(position).getGamer1score());
                String otherscore =Integer.toString(waitinggames.get(position).getGamer2score());
                Intent gameintent =new Intent(getActivity(), GamepageActivity.class);
                startActivity(gameintent);
                waitingAdaptor.notifyDataSetChanged();
            }
        });
        yourturnAdaptor.setOnItemClickListener(new GameAdaptor.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                String rivalid =  yourturngames.get(position).getGamer1().getUsername(); //edit this
                String yourscore =Integer.toString(yourturngames.get(position).getGamer1score());
                String otherscore =Integer.toString(yourturngames.get(position).getGamer2score());
                Intent gameintent =new Intent(getActivity(), GamepageActivity.class);
                startActivity(gameintent);
                yourturnAdaptor.notifyDataSetChanged();
            }
        });
        loadwaitinggames();
        loadyourturngames();
        return root;
    }
    public void loadwaitinggames(){
        User u = new User(WelcomActivity.username,"12345");
        Subject a = new Subject("A");
        List<Subject> subjects = new ArrayList<>();
        subjects.add(a);
        Game game1 = new Game(subjects,u,new User("harch","12345"));
        Game game2 = new Game(subjects,u,new User("yeki","12345"));
        waitinggames.add(game1);
        waitinggames.add(game2);

        waitingAdaptor.setGames(waitinggames);
    }
    public void loadyourturngames(){
        User u = new User(WelcomActivity.username,"12345");
        Subject a = new Subject("A");
        List<Subject> subjects = new ArrayList<>();
        subjects.add(a);
        Game game1 = new Game(subjects,u,new User("chendler","12345"));
        Game game2 = new Game(subjects,u,new User("burney","12345"));
        yourturngames.add(game1);
        yourturngames.add(game2);

        yourturnAdaptor.setGames(yourturngames);
    }

}
