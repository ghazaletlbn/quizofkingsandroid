package com.example.quizofkings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SigninActivity extends AppCompatActivity {
    static EditText username;
    EditText password;
    Button buttonSignIn;
    TextView textView;
    static String signinusername ;
    static String result = "" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        username = findViewById(R.id.editTextUsername);
        password = findViewById(R.id.editTextPassword);
        buttonSignIn = findViewById(R.id.buttonSignIn2);

        buttonSignIn.setOnClickListener(v ->{
            String s = "signIn:" + username.getText().toString()
                    + ":" + password.getText().toString();
            Transfermessage transferMessage = new Transfermessage(SigninActivity.this);
            transferMessage.execute(s);
                if (result.equals("SUCCESS")) {
                    SigninActivity.result = "";
                    SigninActivity.signinusername = username.getText().toString();

                    Intent signintent = new Intent(SigninActivity.this, HomeActivity.class);
                    Toast.makeText(SigninActivity.this, username.getText().toString() + "welcome Back!", Toast.LENGTH_LONG).show();
                    startActivity(signintent);
                } else if (result.equals("ERROR")) {
                    username.requestFocus();
                    username.setError("Wrong Username or Password");
                    Toast.makeText(SigninActivity.this, "Try Again", Toast.LENGTH_LONG).show();
                    SigninActivity.result = "";
                }

        });
    }
}