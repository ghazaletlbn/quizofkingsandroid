package com.example.quizofkings;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    String type;
    Button buttonSignIn;
    Button buttonRegister;
    ImageView imageViewSignIn , imageViewSignUp;
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    static ArrayList<String> users = new ArrayList<>();
    static int width , height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        Log.e("Width", "" + width);
        Log.e("height", "" + height);
        Toast.makeText(MainActivity.this , "width:" + width + "  height:" + height , Toast.LENGTH_LONG).show();


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewSignIn = findViewById(R.id.imageView_signIn);
        imageViewSignUp = findViewById(R.id.imageView_Register);

        imageViewSignIn.setOnClickListener(v -> {

            Intent intent = new Intent(MainActivity.this,SigninActivity.class);
            startActivity(intent);
        });

        imageViewSignUp.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
            startActivity(intent);
        });

    }
}